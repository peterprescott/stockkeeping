from flask import Flask, request, jsonify
from flask_cors import CORS
import sqlite3
import json

app = Flask(__name__)
CORS(app)

@app.route('/')
def hello_world():
    return '<a href="https://treeoflife.org.uk/bookshop">Tree of Life Bookshop</a> API'

@app.route('/add', methods = ['POST'])
def add():
    delivered = request.form['data']
    data = json.loads(delivered)
    conn = sqlite3.connect('books.db')
    c = conn.cursor()
    c.executemany("INSERT INTO Books VALUES (?, ?, ?, ?, ?, ?, ?, ?);", (data,))
    conn.commit()
    conn.close()
    return 'added the book'

@app.route('/move', methods = ['POST'])
def move():
    delivered = request.form['data']
    data = json.loads(delivered)
    newLocation = data[1]
    bookList = data[0]
    conn = sqlite3.connect('books.db')
    c = conn.cursor()
    for book in bookList:
        c.execute('''UPDATE Books SET Location = ? WHERE rowid = ? ''', (newLocation, book))
    conn.commit()
    conn.close()
    return f'changed location of {bookList} to {newLocation}'

@app.route('/remove/isbn/<isbn>', methods = ['POST'])
def removeIsbn(isbn):
    delivered = request.form['data']
    secretkey = json.loads(delivered)
    if (secretkey=='%:c<"%4vTW3ip]I'):
        conn = sqlite3.connect('books.db')
        c = conn.cursor()
        c.execute("SELECT rowid, * FROM Books WHERE ISBN=?", (isbn,))
        rowid = c.fetchone()[0]
        c.execute("DELETE FROM Books WHERE rowid=?", (rowid,))
        conn.commit()
        conn.close()

    return f'removed {isbn}'

@app.route('/remove/rowid/<rowid>', methods = ['POST'])
def removeRowid(rowid):
    delivered = request.form['data']
    secretkey = json.loads(delivered)
    if (secretkey=='%:c<"%4vTW3ip]I'):
        conn = sqlite3.connect('books.db')
        c = conn.cursor()
        c.execute("SELECT rowid, * FROM Books WHERE rowid=?", (rowid,))
        book = c.fetchone()
        c.execute("DELETE FROM Books WHERE rowid=?", (rowid,))
        conn.commit()
        conn.close()

    return f'removed {book}'

@app.route('/search/<field>/<value>')
def search(field, value):
	if (field=='isbn'):
		conn = sqlite3.connect("books.db")
		c = conn.cursor()
		c.execute("SELECT rowid, * FROM Books WHERE ISBN=?", (value,))
		results = c.fetchall()
		conn.commit()
		conn.close()
		return jsonify({"data": results})
	elif (field == 'title'):
		conn = sqlite3.connect("books.db")
		c = conn.cursor()
		c.execute("SELECT rowid, * FROM Books WHERE Title LIKE ?", ('%'+value+'%',))
		results = c.fetchall()
		conn.commit()
		conn.close()
		return jsonify({"data": results})
	elif (field == 'author'):
		conn = sqlite3.connect("books.db")
		c = conn.cursor()
		c.execute("SELECT rowid, * FROM Books WHERE Author LIKE ?", ('%'+value+'%',))
		results = c.fetchall()
		conn.commit()
		conn.close()
		return jsonify({"data": results})
	elif (field == 'publisher'):
		conn = sqlite3.connect("books.db")
		c = conn.cursor()
		c.execute("SELECT rowid, * FROM Books WHERE Publisher LIKE ?", ('%'+value+'%',))
		results = c.fetchall()
		conn.commit()
		conn.close()
		return jsonify({"data": results})
	elif (field == 'location'):
		conn = sqlite3.connect("books.db")
		c = conn.cursor()
		c.execute("SELECT rowid, * FROM Books WHERE Location=?", (value,))
		results = c.fetchall()
		conn.commit()
		conn.close()
		return jsonify({"data": results})

	else: return 'more features coming soon'

