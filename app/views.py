# views.py

from flask import render_template, request, jsonify
import json
import ScannerBrain
import requests
import bs4
import webbrowser

from app import app
app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 0

@app.route('/', methods = ['GET'])
def index():
     return render_template("index.html")

@app.route('/data/<isbn>', methods = ['GET'])
def returnISBNdata(isbn):
	ISBNdata = ScannerBrain.blank(isbn)
	return jsonify({"about": ISBNdata})

webbrowser.open('http://localhost:5000')
