//~ PI Prescott 2018-19.
//~ I write my JavaScript without semicolons.

console.log("CONNECTED!")

function findISBN(){
let isbn = document.getElementById("bookISBN").value
fetch('http://localhost:5000/data/'+isbn)
	   .then(function(response) {
    return response.json();
  })
  .then(function(myJson) {
	  
    let x = myJson
    console.log(x.about)
    document.getElementById("bookTitle").value = x.about.Title
	document.getElementById("bookAuthor").value = x.about.Author
	
	// The next two lines are a quick hack, that presume that the 'publisher' always has a four-digit year of publication preceded by a space and a comma. This seems to be true, but it will probably have exceptions.
	document.getElementById("bookPublisher").value = x.about.Publisher.slice(0,-6)
	document.getElementById("bookPublicationDate").value = x.about.Publisher.slice(-4)
	
	document.getElementById("bookPrice").value = x.about.Price
    return 
  })

}

function findBook() {
	let waitMessage = 'Retrieving Data...'
		document.getElementById("bookTitle").value = waitMessage
		document.getElementById("bookAuthor").value = waitMessage
		document.getElementById("bookPublisher").value = waitMessage
		document.getElementById("bookPublicationDate").value = waitMessage
		document.getElementById("bookPrice").value = waitMessage
	findISBN()

}

function show() {
	// This function just displays the relevant form for the chosen task.
	let choice = document.getElementById('choose').function.value
	document.getElementById('feedback').innerHTML = ""
	document.getElementById('add').style.display='none'
	document.getElementById('remove').style.display='none'
	document.getElementById('search').style.display='none'
	document.getElementById('move').style.display='none'
	document.getElementById(choice).style.display='block'
}

function addBook() {
	// this function needs to check that all the fields are filled in, and then send the jsonified form data to treeoflife.pythonanywhere.com
	// Cloud SQLite3 'Books' Column order: Title,Author,Publisher,Publication Date,ISBN,Price,Location
	let Book = [
	document.getElementById('bookTitle').value,
	document.getElementById('bookAuthor').value,
	document.getElementById('bookPublisher').value,
	document.getElementById('bookPublicationDate').value,
	document.getElementById('bookISBN').value,
	document.getElementById('bookPrice').value,
	document.getElementById('bookPages').value,
	document.getElementById('selectShelf').value
	]
	for (i=0; i<Book.length; i++) {
	if (Book[i] == "" || document.getElementById('selectRoom').value == "" || document.getElementById('selectCase').value == "" || document.getElementById('selectShelf').value == ""){
		document.getElementById('feedback').innerHTML = "<h3>Please fill in all the fields.</h3>"
		return
	}
	}
	targetURL = 'https://treeoflife.pythonanywhere.com/add'
	document.getElementById('feedback').innerHTML = ""
	sendData(Book, targetURL)

}

// this is in progress...

// capacity[i][j] = k implies Room[i], Case [j] has k shelves
const SHOP = [6,6,6,6,6,6,6,6,6,6,6,6]
const B1 = [5, 5, 5,5,5,5,5,7,2,2,2,4,4,4,7,4,4,4,4,4,4,4,4,4,4,4,6,4,4,5,7]
const B2 = [6,5,6,6,7,7,5,7,7,7,4,4,4,4,4,4,4,4]

let roomShelves

function getCases(showHere) {
	room = document.getElementById(showHere+'Room').value
	roomShelves = eval(room)
	document.getElementById(showHere+'Case').innerHTML = "<option value=''>Select Bookcase</option>"
	document.getElementById(showHere+'Shelf').innerHTML = "<option value=''>Select Bookcase First</option>"
	for (i=0; i<roomShelves.length; i++){
	document.getElementById(showHere+'Case').innerHTML += '<option value="'+(i)+'">'+room+'-'+('0'+(i+1)).slice(-2)+'</option>'
	}
}

function getShelves(showHere) {
	bookcase = document.getElementById(showHere+'Case').value
	Shelf = eval(roomShelves[bookcase])
	document.getElementById(showHere+'Shelf').innerHTML = "<option value=''>Select Shelf</option>"
	for (i=0; i<Shelf; i++){
	document.getElementById(showHere+'Shelf').innerHTML += '<option value="'+room+'-'+('0'+(Number(bookcase)+1)).slice(-2)+'-'+(i+1)+'">'+room+'-'+('0'+(Number(bookcase)+1)).slice(-2)+'-'+(i+1)+'</option>'
	}
	
}

// back to normal...

const secretkey= '%:c<"%4vTW3ip]I'

function remove() {
	// this function needs to send a long column of isbns to treeoflife.pythonanywhere.com, where they will be deleted from the database. perhaps with some other record-keeping.
	removeList = document.getElementById('removeList').value
	ISBNlist = removeList.split("\n")
	for (i=0; i<ISBNlist.length; i++) {
		targetURL = 'https://treeoflife.pythonanywhere.com/remove/isbn/'+ISBNlist[i]
		sendData(secretkey, targetURL, ISBNlist[i])
	
	}
	
	
}

function removeBook(rowid) {
		targetURL = 'https://treeoflife.pythonanywhere.com/remove/rowid/' + rowid
		sendData(secretkey, targetURL, rowid)
		let element = document.getElementById(rowid)
		element.parentNode.removeChild(element)
	
}

let foundBooks = []

function search(field){
	let input = document.getElementById(field).value
	document.getElementById(field).value = ""
	let url = 'https://treeoflife.pythonanywhere.com/search/'+field+'/'+input
	console.log(url)
	fetch(url)
	   .then(function(response) {
    return response.json();
  })
  .then(function(myJson) {
	  
    document.getElementById('searchResults').innerHTML = "<p><strong>We found " + myJson.data.length + " results where the " + field + " field includes '" + input + "'.</strong></p>"
    
    foundBooks = myJson.data
    
    for (i=0; i < myJson.data.length; i++) {
		document.getElementById('searchResults').innerHTML += "<p "+"id='"+myJson.data[i][0]+"'>" + myJson.data[i]
		//~ [1].slice(0,20) + "..., " + myJson.data[i][2] + ", £" + myJson.data[i][6] + ", " + myJson.data[i][7] 
		+ "<a style='color: red;' onclick='removeBook(" + myJson.data[i][0] + ")'>[REMOVE]</a></p>"
	}

    return 
  });

}

// adapting search() for location...
function searchLocation(){
	let input = 	document.getElementById('select2Shelf').value
	let url = 'https://treeoflife.pythonanywhere.com/search/'+'location'+'/'+input
	console.log(url)
	fetch(url)
	   .then(function(response) {
    return response.json();
  })
  .then(function(myJson) {
	  
	let shelfListHTML = ''
	  
    shelfListHTML = "<hr><center><p><strong>Select which of these " + myJson.data.length + " books you want to move from " + input + ".</strong></p></center>"
    
    foundBooks = myJson.data
    
    shelfListHTML += "<form id='movingBooks'>"
    
    for (i=0; i < myJson.data.length; i++) {
		shelfListHTML += myJson.data[i]
		//~ [1].slice(0,20) + "..., " + myJson.data[i][2] + ", £" + myJson.data[i][6] + ", " + myJson.data[i][7] 
		+ "<input type='checkbox' name='movingBooks[]' value='" + myJson.data[i][0] + "' checked><br>"
	}
	
	shelfListHTML += ` 
								
								<select class="form-control" id="select3Room" onChange="getCases('select3')">
									<option value="">Select Room</option>
									<option value="SHOP">SHOP</option>
									<option value="B1">B1</option>
									<option value="B2">B2</option>
								</select>
								<select class="form-control" id="select3Case" onChange="getShelves('select3')">
									<option value="">Select Room First</option>
									
								</select>
								<select class="form-control" name="moveLocation" id="select3Shelf" onChange="declareLocation()">
									<option value="">Select Room First</option>
								</select></form><br>
								<center><button class="btn btn-danger" onClick=moveShelf()>Move selected books to <span id="moveLocation">ANOTHER SHELF</span></button></center>
							`
	
	document.getElementById('shelfList').innerHTML = ''
	document.getElementById('shelfList').innerHTML += shelfListHTML
	
    return 
  });

}

function moveShelf(){
	var array = []
	var checkboxes = document.querySelectorAll('input[type=checkbox]:checked')


	for (var i = 0; i < checkboxes.length; i++) {
		array.push(checkboxes[i].value)
	}
	
	let newLocation = document.getElementById('select3Shelf').value
	
	url = 'https://treeoflife.pythonanywhere.com/move'
	
	console.log([array,newLocation])
	
	sendData([array,newLocation],url)
	
}

function declareLocation(){
	document.getElementById('moveLocation').innerText=document.getElementById('select3Shelf').value
	}
// finish adapting.

function sendData(input, target, reaction) {
	
  let data = {'data':JSON.stringify(input)}
	// copied verbatim from https://developer.mozilla.org/en-US/docs/Learn/HTML/Forms/Sending_forms_through_JavaScript
  let XHR = new XMLHttpRequest();
  let urlEncodedData = "";
  let urlEncodedDataPairs = [];
  let name;

  // Turn the data object into an array of URL-encoded key/value pairs.
  for(name in data) {
    urlEncodedDataPairs.push(encodeURIComponent(name) + '=' + encodeURIComponent(data[name]));
  }

  // Combine the pairs into a single string and replace all %-encoded spaces to 
  // the '+' character; matches the behaviour of browser form submissions.
  urlEncodedData = urlEncodedDataPairs.join('&').replace(/%20/g, '+');

  // Define what happens on successful data submission
  XHR.addEventListener('load', function(event) {
		console.log(event.currentTarget)
	if (event.currentTarget.status == "200") {
		document.getElementById('feedback').innerHTML += "<p>" + event.currentTarget.response + "</p>"
	} else { document.getElementById('feedback').innerHTML += "<p>" + reaction + " That didn't seem to work.</p>"}
  });

  // Define what happens in case of error
  XHR.addEventListener('error', function(event) {
    alert('Something went wrong. Is your internet connection working?');
  });

  // Set up our request
  XHR.open('POST', target);

  // Add the required HTTP header for form data POST requests
  XHR.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

  // Finally, send our data.
  XHR.send(urlEncodedData);
}



