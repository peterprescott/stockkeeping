import csv, sqlite3

conn = sqlite3.connect("books.db")
c = conn.cursor()
c.execute('''CREATE TABLE Books
             (Title,Author,Publisher,Publication Date,ISBN,Price,Location)''')

with open('shop-b1-everything.csv', newline='', encoding='cp1252') as csvfile:
	spamreader = csv.reader(csvfile, delimiter=',',quotechar='|')
	for row in spamreader:
			if len(row)==7: 
					print(row)
					c.executemany("INSERT INTO Books VALUES (?, ?, ?, ?, ?, ?, ?);", (row,))
conn.commit()
conn.close()
