# views.py

import sqlite3
from flask import jsonify
from app import app

@app.route('/')
def hello_world():
    return 'Hello from Flask!'

@app.route('/add')
def add():
    return 'add a new book'

@app.route('/remove/<isbn>')
def remove(isbn):
    return f'remove {isbn}'

@app.route('/search/<field>/<value>')
def search(field, value):
	if (field=='isbn'):
		conn = sqlite3.connect("books.db")
		c = conn.cursor()
		c.execute("SELECT * FROM Books WHERE ISBN=?", (value,))
		results = c.fetchall()
		conn.commit()
		conn.close()
		return jsonify({"data": results})
	elif (field == 'title'):
		conn = sqlite3.connect("books.db")
		c = conn.cursor()
		c.execute("SELECT * FROM Books WHERE Title LIKE ?", ('%'+value+'%',))
		results = c.fetchall()
		conn.commit()
		conn.close()
		return jsonify({"data": results})
	elif (field == 'author'):
		conn = sqlite3.connect("books.db")
		c = conn.cursor()
		c.execute("SELECT * FROM Books WHERE Author LIKE ?", ('%'+value+'%',))
		results = c.fetchall()
		conn.commit()
		conn.close()
		return jsonify({"data": results})
	elif (field == 'publisher'):
		conn = sqlite3.connect("books.db")
		c = conn.cursor()
		c.execute("SELECT * FROM Books WHERE Publisher LIKE ?", ('%'+value+'%',))
		results = c.fetchall()
		conn.commit()
		conn.close()
		return jsonify({"data": results})

	else: return 'more features coming soon'

