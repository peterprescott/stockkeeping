//~ PI Prescott 2018-19.
//~ I write my JavaScript without semicolons.

console.log("CONNECTED!")

function findISBN(){
let isbn = document.getElementById("bookISBN").value
fetch('http://localhost:5000/data/'+isbn)
	   .then(function(response) {
    return response.json();
  })
  .then(function(myJson) {
	  
    x = myJson
    console.log(x.about)
    document.getElementById("bookTitle").value = x.about.Title
	document.getElementById("bookAuthor").value = x.about.Author
	document.getElementById("bookPublisher").value = x.about.Publisher
	document.getElementById("bookPrice").value = x.about.Price
    return 
  });

}

function findBook() {
	let waitMessage = 'Retrieving Data...'
		document.getElementById("bookTitle").value = waitMessage
		document.getElementById("bookAuthor").value = waitMessage
		document.getElementById("bookPublisher").value = waitMessage
		document.getElementById("bookPrice").value = waitMessage
	findISBN()

}

function show() {
	// This function just displays the relevant form for the chosen task.
	let choice = document.getElementById('choose').function.value
	console.log(choice)
	document.getElementById('add').style.display='none'
	document.getElementById('remove').style.display='none'
	document.getElementById('search').style.display='none'
	document.getElementById(choice).style.display='block'
}

function addBook() {
	// this function needs to check that all the fields are filled in, and then send the jsonified form data to treeoflife.pythonanywhere.com
	
}

function remove() {
	// this function needs to send a long column of isbns to treeoflife.pythonanywhere.com, where they will be deleted from the database. perhaps with some other record-keeping.
}

function search(field){
	// this function needs to find the value of whatever 'field' (isbn, title, author, publisher) and then ask the cloud database at treeoflife.pythonanywhere.com whether there is anything that matches that request. and then the various results need to be displayed somehow in the browser.
}

function sendData(input, target) {
	
  let data = {'data':JSON.stringify(input)}
	// copied verbatim from https://developer.mozilla.org/en-US/docs/Learn/HTML/Forms/Sending_forms_through_JavaScript
  let XHR = new XMLHttpRequest();
  let urlEncodedData = "";
  let urlEncodedDataPairs = [];
  let name;

  // Turn the data object into an array of URL-encoded key/value pairs.
  for(name in data) {
    urlEncodedDataPairs.push(encodeURIComponent(name) + '=' + encodeURIComponent(data[name]));
  }

  // Combine the pairs into a single string and replace all %-encoded spaces to 
  // the '+' character; matches the behaviour of browser form submissions.
  urlEncodedData = urlEncodedDataPairs.join('&').replace(/%20/g, '+');

  // Define what happens on successful data submission
  XHR.addEventListener('load', function(event) {
    //~ alert('Yeah! Data sent and response loaded.');
  });

  // Define what happens in case of error
  XHR.addEventListener('error', function(event) {
    alert('Oops! Something goes wrong.');
  });

  // Set up our request
  XHR.open('POST', '/'+target);

  // Add the required HTTP header for form data POST requests
  XHR.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

  // Finally, send our data.
  XHR.send(urlEncodedData);
}



