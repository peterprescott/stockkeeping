import requests
import json
import datetime
from random import randint
from bs4 import BeautifulSoup

def blank(x):
#instead of previous non-attempt [below], use requests and beautiful soup to get author, title, publisher, publication date from
	jbquestion = 'https://www.justbooks.co.uk/search/?author=&title=&lang=en&isbn='+x+'&new=1&used=1&ebooks=1&destination=gb&currency=GBP&mode=basic&st=sr&ac=qr'
	print('OpenLibrary.org does not have the data. Give us a minute while we look elsewhere...')
	jbanswer = requests.get(jbquestion, headers={'Accept': 'text/plain'})
	jbtxt = jbanswer.text
	soup = BeautifulSoup(jbtxt, 'html.parser')

	soupauthor = soup.find_all(attrs={'itemprop':"author"})
	souptitle = soup.find_all(attrs={'itemprop':"name"})
	souppublisher = soup.find_all(attrs={'itemprop':"publisher"})
	soupprices = soup.find_all(attrs={'class':"results-price"})
	
	allprices = []
	for soupprice in soupprices:
		price = soupprice.get_text()
		number=''
		for char in price:
			if char in '0123456789.':
				number += char
		cost = float(number)
		allprices.append(cost)
		
	if len(allprices)>1: 
		min_price = min(allprices)
	else:
		min_price = "unknown"

	if bool(soupauthor)==True:
		print('That worked!')
		soupauthor = soupauthor[0].get_text()
		souptitle = souptitle[0].get_text()
		souppublisher= souppublisher[0].get_text()
	else:
		print('Alas, unsuccesful. Are you sure that number was an ISBN?')
		soupauthor = 'Not Found'
		souptitle = ''
		souppublisher = ''

	soupauthor=soupauthor.replace(',','')
	souptitle=souptitle.replace(',','')

	souprrp = ''
	now=str(datetime.datetime.now())
	writesoup = str(soup)
	BookData = {"ISBN":x,"Author":soupauthor,"Publisher":souppublisher,"Title":souptitle, "Price":min_price}
	return BookData
